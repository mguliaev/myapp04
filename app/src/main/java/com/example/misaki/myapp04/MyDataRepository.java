package com.example.misaki.myapp04;

public class MyDataRepository {
    static MyPersonData[] getDataset() {
        MyPersonData[] dataset = new MyPersonData[7];

        dataset[0] = new MyPersonData();
        dataset[0].mName = "Иванов А.И.";
        dataset[0].mPaymentData = new PaymentData[]{
                new PaymentData("01.01.2018", "1001 Р"),
                new PaymentData("01.02.2018", "1002 Р"),
                new PaymentData("01.03.2018", "1003 Р"),
                new PaymentData("01.04.2018", "1004 Р"),
                new PaymentData("01.05.2018", "1005 Р"),
                new PaymentData("01.06.2018", "1006 Р"),
                new PaymentData("01.07.2018", "1007 Р"),
                new PaymentData("01.08.2018", "1008 Р")
        };

        dataset[1] = new MyPersonData();
        dataset[1].mName = "Сидоров В.А.";
        dataset[1].mPaymentData = new PaymentData[]{
                new PaymentData("01.01.2018", "2001 Р"),
                new PaymentData("01.02.2018", "2002 Р"),
                new PaymentData("01.03.2018", "2003 Р"),
                new PaymentData("01.04.2018", "2004 Р"),
                new PaymentData("01.05.2018", "2005 Р"),
                new PaymentData("01.06.2018", "2006 Р"),
                new PaymentData("01.07.2018", "2007 Р"),
                new PaymentData("01.08.2018", "2008 Р"),
                new PaymentData("01.08.2018", "2009 Р")
        };

        dataset[2] = new MyPersonData();
        dataset[2].mName = "Кузнецов А.М.";
        dataset[2].mPaymentData = new PaymentData[0];

        dataset[3] = new MyPersonData();
        dataset[3].mName = "Илларионов М.В.";
        dataset[3].mPaymentData = new PaymentData[0];

        dataset[4] = new MyPersonData();
        dataset[4].mName = "Швабрин А.С.";
        dataset[4].mPaymentData = new PaymentData[0];

        dataset[5] = new MyPersonData();
        dataset[5].mName = "Гринев А.Д.";
        dataset[5].mPaymentData = new PaymentData[0];

        dataset[6] = new MyPersonData();
        dataset[6].mName = "Башаров А.С.";
        dataset[6].mPaymentData = new PaymentData[0];

        return dataset;
    }
}
