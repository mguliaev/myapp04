package com.example.misaki.myapp04;

class PaymentData {
    public String mDate;
    public String mValue;

    public PaymentData(String sDate, String sValue) {
        mDate = sDate;
        mValue = sValue;
    }
}
