package com.example.misaki.myapp04;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MyPersonAdapter extends RecyclerView.Adapter<MyPersonAdapter.ViewHolder> {
    private Activity mActivity;
    private MyPersonData[] mDataset;
    //private MyPaymentAdapter mMyPaymentAdapter;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public Activity mActivity;
        public View mV;
        public TextView mTextView;
        public RecyclerView mRecyclerViewPayments;

        public ViewHolder(Activity activity, View v, TextView textView, RecyclerView recyclerViewPayments) {
            super(v);
            mActivity = activity;
            mV = v;
            mTextView = textView;
            mRecyclerViewPayments = recyclerViewPayments;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyPersonAdapter(MyPersonData[] myDataset, Activity activity) {
        mDataset = myDataset;
        mActivity = activity;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyPersonAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v = layoutInflater.inflate(R.layout.person_view, parent, false);
        TextView textViewValue = (TextView) v.findViewById(R.id.textViewValue);
        RecyclerView recyclerViewPayments = (RecyclerView) v.findViewById(R.id.recyclerViewPayments);
        recyclerViewPayments.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false));
        ViewHolder vh = new ViewHolder(mActivity, v, textViewValue, recyclerViewPayments);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mTextView.setText(mDataset[position].mName);

        //holder.mRecyclerViewPayments.setLayoutManager(new LinearLayoutManager(mActivity));
        MyPaymentAdapter myPaymentAdapter = new MyPaymentAdapter(mDataset[position].mPaymentData);
        holder.mRecyclerViewPayments.setAdapter(myPaymentAdapter);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}

