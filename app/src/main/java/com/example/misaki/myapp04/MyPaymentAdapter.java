package com.example.misaki.myapp04;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MyPaymentAdapter extends RecyclerView.Adapter<MyPaymentAdapter.ViewHolder> {
    private PaymentData[] mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View mV;
        public TextView mTextViewPaymentDate;
        public TextView mTextViewPaymentValue;
        RecyclerView mRecyclerView;

        public ViewHolder(View v, TextView textViewPaymentDate, TextView textViewPaymentValue,
                          RecyclerView recyclerView) {
            super(v);
            mV = v;
            mTextViewPaymentDate = textViewPaymentDate;
            mTextViewPaymentValue = textViewPaymentValue;
            mRecyclerView = recyclerView;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyPaymentAdapter(PaymentData[] myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyPaymentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        // create a new view
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v = layoutInflater.inflate(R.layout.payment_view, parent, false);
        TextView textViewPaymentDate = (TextView) v.findViewById(R.id.textViewPaymentDate);
        TextView textViewPaymentValue = (TextView) v.findViewById(R.id.textViewPaymentValue);
        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.recyclerViewPayments);
        //TextView v = (TextView)inflatedView;
        //ViewHolder vh = new ViewHolder(v);
        ViewHolder vh = new ViewHolder(v, textViewPaymentDate, textViewPaymentValue, recyclerView);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mTextViewPaymentDate.setText(mDataset[position].mDate);
        holder.mTextViewPaymentValue.setText(mDataset[position].mValue);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}

